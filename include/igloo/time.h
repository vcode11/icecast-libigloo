/* Copyright (C) 2021       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifndef _LIBIGLOO__TIME_H_
#define _LIBIGLOO__TIME_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stdbool.h>
#include <time.h>
#include <igloo/types.h>

typedef struct {
    uint32_t flags;
    uint32_t ssec;
    uint64_t fsec;
} igloo_ctime_t;

typedef uint32_t igloo_clock_t;

#define igloo_CLOCK_REALTIME    ((igloo_clock_t)1)
#define igloo_CLOCK_MONOTONIC   ((igloo_clock_t)2)

igloo_error_t   igloo_ctime_from_null(igloo_ctime_t *dst);
igloo_error_t   igloo_ctime_from_time_t(igloo_ctime_t *dst, time_t src, igloo_clock_t clock);
igloo_error_t   igloo_ctime_from_interval(igloo_ctime_t *dst, uint64_t sec, uint32_t nsec, igloo_clock_t clock);
igloo_error_t   igloo_ctime_from_now(igloo_ctime_t *dst, igloo_clock_t clock);
igloo_error_t   igloo_ctime_to_time_t(time_t *dst, igloo_ctime_t src);
igloo_error_t   igloo_ctime_sleep(igloo_ctime_t t);
bool            igloo_ctime_is_null(igloo_ctime_t t);
bool            igloo_ctime_is_valid(igloo_ctime_t t);
bool            igloo_ctime_is_interval(igloo_ctime_t t);
bool            igloo_ctime_is_absolute(igloo_ctime_t t);
bool            igloo_ctime_is_negative(igloo_ctime_t t);

#ifdef __cplusplus
}
#endif

#endif /* ! _LIBIGLOO__TIME_H_ */
