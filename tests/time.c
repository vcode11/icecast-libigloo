#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <igloo/tap.h>
#include <igloo/error.h>
#include <igloo/time.h>

static void test_align(igloo_ctime_t t) {
    union {
        igloo_ctime_t ctime;
        void *vp;
    } ctime = {.ctime = t};

    igloo_tap_test("marked correctly as non-ro", ((uintmax_t)ctime.vp) & 0x1);
}

static void test(time_t t)
{
    igloo_ctime_t ctime;
    time_t r;

    igloo_tap_test_success("igloo_ctime_from_time_t", igloo_ctime_from_time_t(&ctime, t, igloo_CLOCK_REALTIME));
    igloo_tap_test("!igloo_ctime_is_null", !igloo_ctime_is_null(ctime));
    igloo_tap_test("!igloo_ctime_is_interval", !igloo_ctime_is_interval(ctime));
    igloo_tap_test("igloo_ctime_is_absolute", igloo_ctime_is_absolute(ctime));
    igloo_tap_test_success("igloo_ctime_to_time_t", igloo_ctime_to_time_t(&r, ctime));
    igloo_tap_test("t == r", t == r);
    test_align(ctime);
}

static void test_now(void)
{
    igloo_ctime_t ctime;
    time_t now = time(NULL);
    time_t r;

    igloo_tap_test_success("igloo_ctime_from_now", igloo_ctime_from_now(&ctime, igloo_CLOCK_REALTIME));
    igloo_tap_test_success("igloo_ctime_to_time_t", igloo_ctime_to_time_t(&r, ctime));
    igloo_tap_test("r == now", r == now || r == (now - 1));
    test_align(ctime);
}

static void test_interval(uint64_t sec, uint32_t nsec)
{
    igloo_ctime_t ctime;
    time_t r;

    igloo_tap_test_success("igloo_ctime_from_interval", igloo_ctime_from_interval(&ctime, sec, nsec, igloo_CLOCK_MONOTONIC));
    igloo_tap_test("!igloo_ctime_is_null", !igloo_ctime_is_null(ctime));
    igloo_tap_test("igloo_ctime_is_interval", igloo_ctime_is_interval(ctime));
    igloo_tap_test("!igloo_ctime_is_absolute", !igloo_ctime_is_absolute(ctime));
    igloo_tap_test_success("igloo_ctime_to_time_t", igloo_ctime_to_time_t(&r, ctime));
    igloo_tap_test("sec == r", (time_t)sec == r);
    test_align(ctime);
    igloo_tap_test_success("igloo_ctime_sleep", igloo_ctime_sleep(ctime));
}

static const time_t vec[] = {
    0, 1, -1, 2147483647, -2147483648
};

static const struct {
    uint64_t sec;
    uint32_t nsec;
} vec_interval[] = {
    {0, 0},
    {1, 0},
    {0, 1}
};

int main(void)
{
    igloo_ctime_t ctime;
    time_t r;
    size_t i;

    igloo_tap_init();

    igloo_tap_diagnostic("Testing null");
    igloo_tap_test("sizeof(igloo_ctime_t) == 16", sizeof(igloo_ctime_t) == 16);

    igloo_tap_test_success("igloo_ctime_from_null", igloo_ctime_from_null(&ctime));
    igloo_tap_test("igloo_ctime_is_null", igloo_ctime_is_null(ctime));
    igloo_tap_test("!igloo_ctime_is_interval", !igloo_ctime_is_interval(ctime));
    igloo_tap_test("!igloo_ctime_is_absolute", !igloo_ctime_is_absolute(ctime));
    igloo_tap_test_error("igloo_ctime_to_time_t", igloo_ERROR_DOM, igloo_ctime_to_time_t(&r, ctime));
    test_align(ctime);

    igloo_tap_diagnostic("Testing vectors");
    for (i = 0; i < (sizeof(vec)/sizeof(*vec)); i++)
        test(vec[i]);

    for (i = 0; i < (sizeof(vec_interval)/sizeof(*vec_interval)); i++)
        test_interval(vec_interval[i].sec, vec_interval[i].nsec);

    igloo_tap_diagnostic("Testing now");
    test_now();

    igloo_tap_fin();
    return 0;
}
