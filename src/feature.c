/* Copyright (C) 2020       Philipp "ph3-der-loewe" Schafft <lion@lion.leolix.org>
 *
 * This library is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Library General Public
 * License as published by the Free Software Foundation; either
 * version 2 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Library General Public License for more details.
 *
 * You should have received a copy of the GNU Library General Public
 * License along with this library; if not, write to the
 * Free Software Foundation, Inc., 51 Franklin St, Fifth Floor,
 * Boston, MA  02110-1301, USA.
 */

#ifdef HAVE_CONFIG_H
#include <config.h>
#endif

#include <igloo/feature.h>
#include <igloo/error.h>

static inline bool is_valid(igloo_feature_t *feature)
{
    if (!feature)
        return false;

    return (igloo_CONTROL_CHECK3(feature->control, igloo_FEATURE__CONTROL_VERSION, sizeof(igloo_feature_t), igloo_CONTROL_TYPE__FEATURE) && feature->feature_name) ? true : false;
}

igloo_error_t   igloo_feature_get_name(igloo_feature_t *feature, const char **name)
{
    if (!feature || !name)
        return igloo_ERROR_FAULT;

    if (!is_valid(feature))
        return igloo_ERROR_TYPEMM;

    *name = feature->feature_name;

    return igloo_ERROR_NONE;
}

bool            igloo_feature_equal(igloo_feature_t *a, igloo_feature_t *b)
{
    if (!a || !b)
        return false;

    if (a == b)
        return true;

    return false;
}
